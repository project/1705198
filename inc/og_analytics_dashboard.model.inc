<?php

/**
 * @file og_analytics_dashboard.model.inc
 * Functions for dashboard page .
 */


/**
 * Return count of members in a group.
 *
 * @param mixed $node
 * 	 expects either a $node object or a node id of a group.
 * @return int
 *   memberships count.
 */
function og_analytics_group_members_count($node = NULL) {
  if (is_numeric($node))
    $nid = $node;

  if (isset($node->nid))
    $nid = $node->nid;

  if (!$nid)
 		return FALSE;

  $ret = db_result(db_query('SELECT COUNT(DISTINCT(uid)) as count FROM {og_uid} WHERE nid = %d', (int) $nid));
  return (int) $ret;

}


/**
 * Return count of posts in a group.
 *
 * @param mixed $node
 * 	 expects either a $node object or a node id of a group.
 * @param bool $comments
 *   include comments (true by default).
 * @param array $types
 * 	 specify the node types that will be counted. If nothing specified,
 * 	 which is default, no filtering on node types will be made.
 * @return int
 *   memberships count.
 */
function og_analytics_group_posts_count($node = NULL, $comments = TRUE, $types = array()) {
  if (is_numeric($node))
    $nid = $node;

  if (isset($node->nid))
    $nid = $node->nid;

  if (!$nid)
  	return FALSE;

	$select = 'COUNT(oga.nid) AS count';
  $join = '';

  if ($comments == TRUE) {
  	$select = '(COUNT(oga.nid)+SUM(ncs.comment_count)) AS count';
  	$join = 'JOIN node_comment_statistics ncs ON ncs.nid = oga.nid';
  }
  
  if ($types) {
		$query = 
		'	SELECT ' . $select . '
		 	FROM {og_ancestry} oga
			JOIN {node} n ON n.nid = oga.nid '
			. $join . '
			WHERE
				oga.group_nid = %d
			AND
				n.type IN (' . db_placeholders($types, 'varchar') . ')
		';
		$ret = db_result(db_query($query, array_merge(array($nid), $types)));

	} else {

		$query = 
		'	SELECT ' . $select . '
			FROM {og_ancestry} oga '
			. $join . '
			WHERE
				oga.group_nid = %d
		';
		$ret = db_result(db_query($query, $nid));
	}

  return (int) $ret;

}



/**
 * Return count of posts in previuos period and count of posts in current period.
 *
 * @param mixed $node
 * 	 expects either a $node object or a node id of a group.
 * @param bool $comments
 *   include comments (true by default).
 * @param array $types
 * 	 specify the node types that will be counted. If nothing specified,
 * 	 which is default, no filtering on node types will be made.
 * @return int
 *   memberships count.
 */
function og_analytics_posts_period_trend($node = NULL, $comments = TRUE, $types = array(), $period = 'month') {
  if (is_numeric($node))
    $nid = $node;

  if (isset($node->nid))
    $nid = $node->nid;

  if (!$nid)
  	return FALSE;

  if (!$types || empty($types))
  	$types = FALSE;

  if ($types) {
		$types_sql = '
		AND
		n.type IN (' . db_placeholders($types, 'varchar') . ')
		';
	}


  $query = 'SELECT
	(
		SELECT (COUNT(DISTINCT(n.nid)) + SUM(ncs.comment_count)) as prev_month
		FROM node n
		JOIN og_ancestry oga ON n.nid = oga.nid
		JOIN node_comment_statistics ncs ON n.nid = ncs.nid
		WHERE
		n.created BETWEEN 
			UNIX_TIMESTAMP(DATE_FORMAT(NOW() - INTERVAL 1 MONTH, "%Y-%m-01"))
			AND
			UNIX_TIMESTAMP(LAST_DAY(NOW() - INTERVAL 1 MONTH ))
		AND
		oga.group_nid = %d
		' . $types_sql . '
	) as prev_period,
	(
		SELECT (COUNT(DISTINCT(n.nid)) + SUM(ncs.comment_count)) as curr_period
		FROM node n
		JOIN og_ancestry oga ON n.nid = oga.nid
		JOIN node_comment_statistics ncs ON n.nid = ncs.nid
		WHERE
		n.created BETWEEN 
			UNIX_TIMESTAMP(DATE_FORMAT(NOW(), "%Y-%m-01"))
			AND
			UNIX_TIMESTAMP(LAST_DAY(NOW()))
		AND
		oga.group_nid = %d
		' . $types_sql . '
	) as curr_period
  ';

  if ($types) {
  	$ret = db_fetch_array(db_query($query, array_merge(
	  		array($nid), $types, array($nid), $types)
	  	));
	} else {
		$ret = db_fetch_array(db_query($query, $nid, $nid)
	  	);
	}

	// Initialize variables.
	$prev_period = (int) $ret['prev_period'];
	$curr_period = (int) $ret['curr_period'];
	$percent = 0;
	$trend = 0;

	if ($prev_period != 0 && $prev_period != NULL) {
		$number = ($curr_period/$prev_period);

		if ($number == 1) {
			$percent = 0;
			$trend = 0;
		}

		if ($number < 1) {
			$percent = number_format((1-($curr_period/$prev_period))*100, 0);
			$trend = -1;
		}

		if ($number > 1) {
			$percent = number_format(($curr_period/$prev_period)*100, 0);
			$trend = 1;
		}

	}

	return array(
		'prev_period' => $prev_period,
		'curr_period' => $curr_period,
		'percent' => $percent,
		'trend' => $trend,
	);



}