
// has to be loaded outside of drupal behaviors
google.load("visualization", "1", {packages:["corechart", "table"]});


// theme the table layout of google charts generated table listing
var cssClassNames_val = {
  tableRow : 'og_analytics_table_hr_row', 
  headerRow: 'og_analytics_table_hr_row_header',
  selectedTableRow : 'og_analytics_table_row_selected',
  headerCell : 'og_analytics_table_hr_row',
  tableCell : 'og_analytics_table_hr_row',
  hoverTableRow: 'og_analytics_table_row_hover',
  oddTableRow : 'og_analytics_table_hr_row_odd'
};


Drupal.behaviors.og_analytics_navigation = function (context) {

	$('#og_analytics_navigation').bind('change', function(e) {
		document.location.href=$(this).val();
	});
}