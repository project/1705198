
Drupal.behaviors.og_analytics_downloads = function (context) {

  draw_top_downloaded_content_distrib_form();
  draw_top_downloaded_content_form();
  draw_top_downloads_employee_status_form();
  draw_top_downloading_users_form();

  $('#edit-top-downloaded-content-submit').click(function() {
    draw_top_downloaded_content_form();
    return false;
  });

  $('#edit-top-downloaded-content-distrib-submit').click(function() {
    draw_top_downloaded_content_distrib_form();
    return false;
  });

  $('#edit-top-downloads-employee-status-submit').click(function() {
    draw_top_downloads_employee_status_form();
    return false;
  });

  $('#edit-top-downloading-users-submit').click(function() {
    draw_top_downloading_users_form();
    return false;
  });

  $('#edit-file-download-users-submit').click(function() {
    draw_file_download_users_form();
    return false;
  });


}

  var cssClassNames_val = {
    tableRow : 'og_analytics_table_hr_row', 
    headerRow: 'og_analytics_table_hr_row_header',
    selectedTableRow : 'og_analytics_table_row_selected',
    headerCell : 'og_analytics_table_hr_row',
    tableCell : 'og_analytics_table_hr_row',
    hoverTableRow: 'og_analytics_table_row_hover',
    oddTableRow : 'og_analytics_table_hr_row_odd'
  };


  function draw_top_downloads_employee_status_form() {

    var $inputs = $('#og-analytics-top-downloads-employee-status-form :input');

    var nid = document.getElementById('edit-gid');

    $('#top_downloads_employee_status_chart1').css('display', 'none');
    var loading_icon_1 = $('<div class="og_analytics_loading_2"></div>').insertBefore('#top_downloads_employee_status_chart1');

    var jsonData = $.ajax({
      type: 'POST',
      url: "/node/"+nid.value+"/og/analytics/get_top_downloads_employee_status",
      dataType:"json",
      cache: false,
      async: true,
      data: {
        gid: nid.value,
        content_type: 'other_content_file',
        date_start: $("#og-analytics-top-downloads-employee-status-form input[name='date_start[date]']").val(),
        date_end: $("#og-analytics-top-downloads-employee-status-form input[name='date_end[date]']").val()
      },
      success: function(data_in, textStatus, jqXHR) {

        // Create our data table out of JSON data loaded from server.
        var data = new google.visualization.DataTable(data_in);

        var options = {
          title: 'Total Downloads Other Files ',
          titleTextStyle: {color: 'white', fontSize: 14},
          width: 350,
          height: 240,
          legend: {position: 'bottom', textStyle: {color: 'white', fontSize: 14}},
          pieSliceText: 'value',
          backgroundColor: 'black'
        };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('top_downloads_employee_status_chart1'));
        chart.draw(data, options);
        
        loading_icon_1.remove();
        $('#top_downloads_employee_status_chart1').css('display', 'block');
      }
    });
        
    

    /* ----------- second chart */

    $('#top_downloads_employee_status_chart2').css('display', 'none');
    var loading_icon_2 = $('<div class="og_analytics_loading_2"></div>').insertBefore('#top_downloads_employee_status_chart2');

    var jsonData_2 = $.ajax({
      type: 'POST',
      url: "/node/"+nid.value+"/og/analytics/get_top_downloads_employee_status",
      dataType:"json",
      async: true,
      data: {
        gid: nid.value,
        content_type: 'content_file',
        date_start: $("#og-analytics-top-downloads-employee-status-form input[name='date_start[date]']").val(),
        date_end: $("#og-analytics-top-downloads-employee-status-form input[name='date_end[date]']").val()
      },
      success: function(data_in, textStatus, jqXHR) {

        // Create our data table out of JSON data loaded from server.
        var data_2 = new google.visualization.DataTable(data_in);

        var options_2 = {
          title: 'Total Downloads Content Files',
          titleTextStyle: {color: 'white', fontSize: 14},
          width: 350,
          height: 240,
          legend: {position: 'bottom', textStyle: {color: 'white', fontSize: 14}},
          backgroundColor: 'black'
        };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('top_downloads_employee_status_chart2'));
        chart.draw(data_2, options_2);
      
        loading_icon_2.remove();
        $('#top_downloads_employee_status_chart2').css('display', 'block');
      }
    });
    
  }



  function draw_top_downloaded_content_form() {

    var $inputs = $('#og-analytics-top-downloaded-content-form :input');

    var nid = document.getElementById('edit-gid');

    $('#top_downloaded_content_chart').css('display', 'none');
    var loading_icon = $('<div class="og_analytics_loading_2"></div>').insertBefore('#top_downloaded_content_chart');

    var jsonData = $.ajax({
      type: 'POST',
      url: "/node/"+nid.value+"/og/analytics/get_top_downloaded_content",
      dataType:"json",
      async: true,
      data: $inputs,
      success: function(data_in, textStatus, jqXHR) {

        // Create our data table out of JSON data loaded from server.
        var data = new google.visualization.DataTable(data_in);

        data.addColumn('string', 'Action');
        // data.addRows([
        //   ['Mike',  {v: 10000, f: ''}, true]
        // ]);

        var view = new google.visualization.DataView(data);
        view.setColumns([0,3,4]);

        var options = {
          title: 'Top Downloads',
          width: 700,
          height: 240,
          chartArea: {left: 400},
          legend: {position: 'bottom'}
        };

        var table = new google.visualization.Table(document.getElementById('top_downloaded_content_chart'));
        
        //var formatter_pattern = new google.visualization.PatternFormat('<a href="/node/{1}">{0}</a>');
        //formatter_pattern.format(data, [0, 1]); // Apply formatter and set the formatted value of the first column.

        //var formatter_pattern = new google.visualization.PatternFormat('View Users');
        var formatter_pattern = new google.visualization.PatternFormat('<a href="javascript:return true;">View Users</a>');
        formatter_pattern.format(data, [3,2], 4); // Apply formatter and set the formatted value of the first column.

        var formatter_bar = new google.visualization.BarFormat({width: 120});
        formatter_bar.format(data, 3); // apply formatter to second column
      
        table.draw(view, {width: 'auto', allowHtml: true, showRowNumber: true, cssClassNames: cssClassNames_val});

        loading_icon.remove();
        $('#top_downloaded_content_chart').css('display', 'block');

        google.visualization.events.addListener(table, 'select', selectHandler); 

        function selectHandler(e) {

          var file_name = data.getValue(table.getSelection()[0].row, 0);
          $("#og-analytics-file-download-users-form input[name='file_name']").val(file_name);

          draw_file_download_users_form();

          $(document.body).animate({
            'scrollTop':   $('#og-analytics-file-download-users-form').offset().top
          }, 2000);

        }

      }
    });
        
    


  }



  function draw_top_downloaded_content_distrib_form() {

    var $inputs = $('#og-analytics-top-downloaded-content-distrib-form :input');

    var nid = document.getElementById('edit-gid');

    $('#top_downloaded_content_distrib_chart').css('display', 'none');
    var loading_icon = $('<div class="og_analytics_loading_2"></div>').insertBefore('#top_downloaded_content_distrib_chart');

    var jsonData = $.ajax({
      type: 'POST',
      url: "/node/"+nid.value+"/og/analytics/get_downloaded_content_distrib",
      dataType:"json",
      async: true,
      data: $inputs,
      success: function(data_in, textStatus, jqXHR) {
        // Create our data table out of JSON data loaded from server.
        var data = new google.visualization.DataTable(data_in);

        var view = new google.visualization.DataView(data);
        view.setColumns([0,2,3,4]);

         options = {
          title: 'Top Downloads by community',
          width: 700,
          height: 240,
          chartArea: {left: 400},
          legend: {position: 'bottom'}
        };

        // Instantiate and draw our chart, passing in some options.
        //var chart = new google.visualization.BarChart(document.getElementById('top_downloaded_content_distrib_form'));
        //chart.draw(data, options);

        var table = new google.visualization.Table(document.getElementById('top_downloaded_content_distrib_chart'));

        var formatter_pattern = new google.visualization.PatternFormat('<a href="/node/{1}">{0}</a>');
        formatter_pattern.format(data, [0, 1]); // Apply formatter and set the formatted value of the first column.

        var formatter_bar = new google.visualization.BarFormat({width: 120});
        formatter_bar.format(data, 2); // apply formatter to second column
        formatter_bar.format(data, 3); // apply formatter to third column

        table.draw(view, {width: 'auto', allowHtml: true, showRowNumber: true, cssClassNames: cssClassNames_val});

        loading_icon.remove();
        $('#top_downloaded_content_distrib_chart').css('display', 'block');
      }
    });
        
    

  }


  function draw_top_downloading_users_form() {

    var $inputs = $('#og-analytics-top-downloading-users-form :input');

    var nid = document.getElementById('edit-gid');

    $('#top_downloading_users_chart').css('display', 'none');
    var loading_icon = $('<div class="og_analytics_loading_2"></div>').insertBefore('#top_downloading_users_chart');

    var download_type_val = 0;
    if ($("#og-analytics-top-downloading-users-form input[name=download_type]").is(':checked')) {
      download_type_val = 1;
    } else {
      download_type_val = 0;
    }

    var jsonData = $.ajax({
      type: 'POST',
      url: "/node/"+nid.value+"/og/analytics/get_top_downloading_users",
      dataType:"json",
      async: true,
      //data: $inputs
      data: {
        gid: nid.value,
        download_type: download_type_val,
        date_start: $("#og-analytics-top-downloading-users-form input[name='date_start[date]']").val(),
        date_end: $("#og-analytics-top-downloading-users-form input[name='date_end[date]']").val(),
        limit: $("#og-analytics-top-downloading-users-form select[name='limit']").val()
      },
      success: function(data_in, textStatus, jqXHR) {
        // Create our data table out of JSON data loaded from server.
        var data = new google.visualization.DataTable(data_in);

        var view = new google.visualization.DataView(data);
        view.setColumns([0,2,3,4]);

        // Instantiate and draw our chart, passing in some options.
        //var chart = new google.visualization.BarChart(document.getElementById('top_downloaded_content_distrib_form'));
        //chart.draw(data, options);

        var table = new google.visualization.Table(document.getElementById('top_downloading_users_chart'));
        
        var formatter_pattern = new google.visualization.PatternFormat('<a href="/user/{1}">{0}</a>');
        formatter_pattern.format(data, [0, 1]); // Apply formatter and set the formatted value of the first column.

        var formatter_bar = new google.visualization.BarFormat({width: 120});
        formatter_bar.format(data, 4); // apply formatter to second column

        table.draw(view, {width: 'auto', allowHtml: true, showRowNumber: true, cssClassNames: cssClassNames_val});

        loading_icon.remove();
        $('#top_downloading_users_chart').css('display', 'block');
      }
    });

  }


  function draw_file_download_users_form() {

    var $inputs = $('#og-analytics-file-download-users-form :input');

    var nid = document.getElementById('edit-gid');

    $('#file_download_users_chart').css('display', 'none');
    var loading_icon = $('<div class="og_analytics_loading_2"></div>').insertBefore('#file_download_users_chart');

    var jsonData = $.ajax({
      type: 'POST',
      url: "/node/"+nid.value+"/og/analytics/get_file_download_users",
      dataType:"json",
      async: true,
      //data: $inputs
      data: {
        gid: nid.value,
        date_start: $("#og-analytics-file-download-users-form input[name='date_start[date]']").val(),
        date_end: $("#og-analytics-file-download-users-form input[name='date_end[date]']").val(),
        limit: $("#og-analytics-file-download-users-form select[name='limit']").val(),
        file_name: $("#og-analytics-file-download-users-form input[name='file_name']").val()
      },
      success: function(data_in, textStatus, jqXHR) {
        // Create our data table out of JSON data loaded from server.
        var data = new google.visualization.DataTable(data_in);

        var view = new google.visualization.DataView(data);
        view.setColumns([0,2,3]);

        var table = new google.visualization.Table(document.getElementById('file_download_users_chart'));
        
        var formatter_pattern = new google.visualization.PatternFormat('<a href="/user/{1}">{0}</a>');
        formatter_pattern.format(data, [0, 1]); // Apply formatter and set the formatted value of the first column.

        table.draw(view, {width: 'auto', allowHtml: true, showRowNumber: true, cssClassNames: cssClassNames_val});

        loading_icon.remove();
        $('#file_download_users_chart').css('display', 'block');
      }
    });


     
      /*   
    // Create our data table out of JSON data loaded from server.
    var data = new google.visualization.DataTable(jsonData);

    var view = new google.visualization.DataView(data);
    view.setColumns([0,2,3]);

    var table = new google.visualization.Table(document.getElementById('file_download_users_chart'));
    
    var formatter_pattern = new google.visualization.PatternFormat('<a href="/user/{1}">{0}</a>');
    formatter_pattern.format(data, [0, 1]); // Apply formatter and set the formatted value of the first column.

    table.draw(view, {width: 'auto', allowHtml: true, showRowNumber: true, cssClassNames: cssClassNames_val});
    */
  }
