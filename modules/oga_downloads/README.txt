OG Analytics - Downloads report extension

Requirements:
1. download_count module must be installed
2. private file system should be used, so that download requests go through drupal and are recorded (admin -> file system -> toggle on private file system mode)