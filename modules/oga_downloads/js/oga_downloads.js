
Drupal.behaviors.oga_downloads = function (context) {

  oga_downloads_get_downloads_by_content_type();
  draw_file_download_users_form();
  oga_downloads_get_top_downloaded_content();
  draw_top_downloading_users_form();

  $('#edit-downloads-by-content-type-submit').click(function() {
    oga_downloads_get_downloads_by_content_type();
    return false;
  });

  $('#edit-top-downloaded-content-submit').click(function() {
    oga_downloads_get_top_downloaded_content();
    return false;
  });

  $('#edit-top-downloading-users-submit').click(function() {
    draw_top_downloading_users_form();
    return false;
  });

  $('#edit-file-download-users-submit').click(function() {
    draw_file_download_users_form();
    return false;
  });


}


  function oga_downloads_get_downloads_by_content_type() {

    var form_id = 'oga-downloads-get-downloads-by-content-type-form';

    var $inputs = $('#'+form_id+' :input');

    var nid = document.getElementById('edit-gid');

    $('#downloads_by_content_type_chart').css('display', 'none');
    var loading_icon = $('<div class="og_analytics_loading_2"></div>').insertBefore('#downloads_by_content_type_chart');

    var content_types = [];
    $('#'+form_id+ " select[name='content_type[]'] option").each(function(i) {
      if (this.selected == true) {
        content_types.push(this.value);
      }
    });

    var jsonData = $.ajax({
      type: 'POST',
      url: Drupal.settings.basePath + "/node/"+nid.value+"/og/analytics/oga_downloads_get_downloads_by_content_type",
      dataType:"json",
      async: true,
      data: {
        gid: nid.value,
        content_type: JSON.stringify(content_types),
        date_start: $("#"+form_id+" input[name='date_start[date]']").val(),
        date_end: $("#"+form_id+" input[name='date_end[date]']").val(),
      },
      
      success: function(data_in, textStatus, jqXHR) {
 
        // Create our data table out of JSON data loaded from server.
        var data = new google.visualization.DataTable(data_in);

        var view = new google.visualization.DataView(data);
        view.setColumns([0,1]);

        var options = {
          title: 'Downloads by Content Type',
          width: 'auto',
          height: 400,
          legend: {position: 'bottom'}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('downloads_by_content_type_chart'));
      
        chart.draw(view, options);

        loading_icon.remove();
        $('#downloads_by_content_type_chart').css('display', 'block');
      }
    });
    
  }


  function draw_top_downloads_employee_status_form() {

    var form_id = 'oga-downloads-top-downloads-employee-status-form';

    var $inputs = $('#'+form_id+' :input');

    var nid = document.getElementById('edit-gid');

    $('#top_downloads_employee_status_chart1').css('display', 'none');
    var loading_icon_1 = $('<div class="og_analytics_loading_2"></div>').insertBefore('#top_downloads_employee_status_chart1');

    var jsonData = $.ajax({
      type: 'POST',
      url: "/node/"+nid.value+"/og/analytics/oga_downloads_get_top_downloads_employee_status",
      dataType:"json",
      cache: false,
      async: true,
      data: {
        gid: nid.value,
        content_type: 'other_content_file',
        date_start: $("#"+form_id+" input[name='date_start[date]']").val(),
        date_end: $("#"+form_id+" input[name='date_end[date]']").val()
      },
      success: function(data_in, textStatus, jqXHR) {

        // Create our data table out of JSON data loaded from server.
        var data = new google.visualization.DataTable(data_in);

        var options = {
          title: 'Total Downloads Other Files ',
          titleTextStyle: {color: 'white', fontSize: 14},
          width: 350,
          height: 240,
          legend: {position: 'bottom', textStyle: {color: 'white', fontSize: 14}},
          pieSliceText: 'value',
          backgroundColor: 'black'
        };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('top_downloads_employee_status_chart1'));
        chart.draw(data, options);
        
        loading_icon_1.remove();
        $('#top_downloads_employee_status_chart1').css('display', 'block');
      }
    });
        
    

    /* ----------- second chart */

    $('#top_downloads_employee_status_chart2').css('display', 'none');
    var loading_icon_2 = $('<div class="og_analytics_loading_2"></div>').insertBefore('#top_downloads_employee_status_chart2');

    var jsonData_2 = $.ajax({
      type: 'POST',
      url: "/node/"+nid.value+"/og/analytics/oga_downloads_get_top_downloads_employee_status",
      dataType:"json",
      async: true,
      data: {
        gid: nid.value,
        content_type: 'content_file',
        date_start: $("#"+form_id+" input[name='date_start[date]']").val(),
        date_end: $("#"+form_id+" input[name='date_end[date]']").val()
      },
      success: function(data_in, textStatus, jqXHR) {

        // Create our data table out of JSON data loaded from server.
        var data_2 = new google.visualization.DataTable(data_in);

        var options_2 = {
          title: 'Total Downloads Content Files',
          titleTextStyle: {color: 'white', fontSize: 14},
          width: 350,
          height: 240,
          legend: {position: 'bottom', textStyle: {color: 'white', fontSize: 14}},
          pieSliceText: 'value',
          backgroundColor: 'black'
        };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('top_downloads_employee_status_chart2'));
        chart.draw(data_2, options_2);
      
        loading_icon_2.remove();
        $('#top_downloads_employee_status_chart2').css('display', 'block');
      }
    });
    
  }



  function oga_downloads_get_top_downloaded_content() {

    var form_id = 'oga-downloads-top-downloaded-content-form';

    var $inputs = $('#'+form_id+' :input');

    var nid = document.getElementById('edit-gid');

    $('#top_downloaded_content_chart').css('display', 'none');
    var loading_icon = $('<div class="og_analytics_loading_2"></div>').insertBefore('#top_downloaded_content_chart');

    var content_types = [];
    $('#'+form_id+ " select[name='content_type[]'] option").each(function(i) {
      if (this.selected == true) {
        content_types.push(this.value);
      }
    });

    var jsonData = $.ajax({
      type: 'POST',
      url: Drupal.settings.basePath + "/node/"+nid.value+"/og/analytics/oga_downloads_get_top_downloaded_content",
      dataType:"json",
      async: true,
      data: {
        gid: nid.value,
        content_type: JSON.stringify(content_types),
        date_start: $("#"+form_id+" input[name='date_start[date]']").val(),
        date_end: $("#"+form_id+" input[name='date_end[date]']").val(),
        limit: $("#"+form_id+" select[name='limit']").val()
      },
      
      success: function(data_in, textStatus, jqXHR) {
 
        // Create our data table out of JSON data loaded from server.
        var data = new google.visualization.DataTable(data_in);

        data.addColumn('string', 'Action');

        var view = new google.visualization.DataView(data);
        view.setColumns([0,3,4]);

        var options = {
          title: 'Top Downloads',
          width: 700,
          height: 240,
          chartArea: {left: 400},
          legend: {position: 'bottom'}
        };

        var table = new google.visualization.Table(document.getElementById('top_downloaded_content_chart'));
        
        //var formatter_pattern = new google.visualization.PatternFormat('<a href="/node/{1}">{0}</a>');
        //formatter_pattern.format(data, [0, 1]); // Apply formatter and set the formatted value of the first column.

        //var formatter_pattern = new google.visualization.PatternFormat('View Users');
        var formatter_pattern = new google.visualization.PatternFormat('<a href="#">View Users</a>');
        formatter_pattern.format(data, [3,2], 4); // Apply formatter and set the formatted value of the first column.

        var formatter_bar = new google.visualization.BarFormat({width: 120});
        formatter_bar.format(data, 3); // apply formatter to second column
      
        table.draw(view, {width: 'auto', allowHtml: true, showRowNumber: true, cssClassNames: cssClassNames_val});

        loading_icon.remove();
        $('#top_downloaded_content_chart').css('display', 'block');

        google.visualization.events.addListener(table, 'select', selectHandler); 

        function selectHandler(e) {

          var file_name = data.getValue(table.getSelection()[0].row, 0);
          var date_start = $("#"+form_id+" input[name='date_start[date]']").val();
          var date_end = $("#"+form_id+" input[name='date_end[date]']").val();
          $("#oga-downloads-file-download-users-form input[name='file_name']").val(file_name);
          $("#oga-downloads-file-download-users-form input[name='date_start[date]']").val(date_start);
          $("#oga-downloads-file-download-users-form input[name='date_end[date]']").val(date_end);

          draw_file_download_users_form();

          $(document.body).animate({
            'scrollTop':   $('#oga-downloads-file-download-users-form').offset().top
          }, 2000);

          return true;

        }

      }
    });
        
    


  }

  

  function draw_top_downloading_users_form() {

    var form_id = 'oga-downloads-top-downloading-users-form';

    var $inputs = $('#'+form_id+' :input');

    var nid = document.getElementById('edit-gid');

    $('#top_downloading_users_chart').css('display', 'none');
    var loading_icon = $('<div class="og_analytics_loading_2"></div>').insertBefore('#top_downloading_users_chart');

    var download_type_val = 0;
    if ($("#"+form_id+" input[name=download_type]").is(':checked')) {
      download_type_val = 1;
    } else {
      download_type_val = 0;
    }

    var jsonData = $.ajax({
      type: 'POST',
      url: Drupal.settings.basePath + "/node/"+nid.value+"/og/analytics/oga_downloads_get_top_downloading_users",
      dataType:"json",
      async: true,
      data: {
        gid: nid.value,
        download_type: download_type_val,
        date_start: $("#"+form_id+" input[name='date_start[date]']").val(),
        date_end: $("#"+form_id+" input[name='date_end[date]']").val(),
        limit: $("#"+form_id+" select[name='limit']").val()
      },
      success: function(data_in, textStatus, jqXHR) {
        // Create our data table out of JSON data loaded from server.
        var data = new google.visualization.DataTable(data_in);

        var view = new google.visualization.DataView(data);
        view.setColumns([0,2,3]);

        // Instantiate and draw our chart, passing in some options.
        //var chart = new google.visualization.BarChart(document.getElementById('top_downloaded_content_distrib_form'));
        //chart.draw(data, options);

        var table = new google.visualization.Table(document.getElementById('top_downloading_users_chart'));
        
        var formatter_pattern = new google.visualization.PatternFormat('<a href="' + Drupal.settings.basePath + '/user/{1}">{0}</a>');
        formatter_pattern.format(data, [0, 1]); // Apply formatter and set the formatted value of the first column.

        var formatter_bar = new google.visualization.BarFormat({width: 120});
        formatter_bar.format(data, 3); // apply formatter to second column

        table.draw(view, {width: 'auto', allowHtml: true, showRowNumber: true, cssClassNames: cssClassNames_val});

        loading_icon.remove();
        $('#top_downloading_users_chart').css('display', 'block');
      }
    });

  }


  function draw_file_download_users_form() {

    var form_id = 'oga-downloads-file-download-users-form';

    var $inputs = $('#'+form_id+' :input');

    var nid = document.getElementById('edit-gid');

    $('#file_download_users_chart').css('display', 'none');
    var loading_icon = $('<div class="og_analytics_loading_2"></div>').insertBefore('#file_download_users_chart');

    var jsonData = $.ajax({
      type: 'POST',
      url: Drupal.settings.basePath + "/node/"+nid.value+"/og/analytics/oga_downloads_get_file_download_users",
      dataType:"json",
      async: true,
      //data: $inputs
      data: {
        gid: nid.value,
        date_start: $("#"+form_id+" input[name='date_start[date]']").val(),
        date_end: $("#"+form_id+" input[name='date_end[date]']").val(),
        limit: $("#"+form_id+" select[name='limit']").val(),
        file_name: $("#"+form_id+" input[name='file_name']").val()        
      },
      success: function(data_in, textStatus, jqXHR) {
        // Create our data table out of JSON data loaded from server.
        var data = new google.visualization.DataTable(data_in);

        var view = new google.visualization.DataView(data);
        view.setColumns([0,2,3]);

        var table = new google.visualization.Table(document.getElementById('file_download_users_chart'));
        
        var formatter_pattern = new google.visualization.PatternFormat('<a href="' + Drupal.settings.basePath + '/user/{1}">{0}</a>');
        formatter_pattern.format(data, [0, 1]); // Apply formatter and set the formatted value of the first column.

        var formatter_bar = new google.visualization.BarFormat({width: 120});
        formatter_bar.format(data, 3); // apply formatter to second column

        table.draw(view, {width: 'auto', allowHtml: true, showRowNumber: true, cssClassNames: cssClassNames_val});

        loading_icon.remove();
        $('#file_download_users_chart').css('display', 'block');
      }
    });
    
  }
