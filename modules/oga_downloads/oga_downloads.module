<?php
/**
 * @file
 * OG Analytics Downloads module.
 */

/**
 * @package OG Analytics Downloads
 * @subpackage OG Analytics
 * @author Liran Tal <liran.tal@gmail.com>
 */


/**
 * Implements hook_menu().
 */
function oga_downloads_menu() {
  // Define downloads reports page.
  $items['node/%node/og/analytics/get_top_downloads_report'] = array(
    'title' => 'report',
    'page callback' => 'oga_downloads_get_top_downloads_report',
    'page arguments' => array(1),
    'access callback' => 'user_access',
    'access arguments' => array('view og downloads analytics'),
    'type' => MENU_LOCAL_TASK,
  );

  // Define downloads callbacks for reports.
  $items['node/%node/og/analytics/oga_downloads_get_top_downloaded_content'] = array(
    'page callback' => 'oga_downloads_get_top_downloaded_content',
    'access callback' => 'user_access',
    'access arguments' => array('view og downloads analytics'),
    'type' => MENU_CALLBACK,
  );


  $items['node/%node/og/analytics/oga_downloads_get_totals_downloads_content_type'] = array(
    'page callback' => 'oga_downloads_get_totals_downloads_content_type',
    'access callback' => 'user_access',
    'access arguments' => array('view og downloads analytics'),
    'type' => MENU_CALLBACK,
  );

  $items['node/%node/og/analytics/oga_downloads_get_top_downloading_users'] = array(
    'page callback' => 'oga_downloads_get_top_downloading_users',
    'access callback' => 'user_access',
    'access arguments' => array('view og downloads analytics'),
    'type' => MENU_CALLBACK,
  );

  $items['node/%node/og/analytics/oga_downloads_get_file_download_users'] = array(
    'page callback' => 'oga_downloads_get_file_download_users',
    'access callback' => 'user_access',
    'access arguments' => array('view og downloads analytics'),
    'type' => MENU_CALLBACK,
  );

  $items['node/%node/og/analytics/oga_downloads_get_downloads_by_content_type'] = array(
    'page callback' => 'oga_downloads_get_downloads_by_content_type',
    'access callback' => 'user_access',
    'access arguments' => array('view og downloads analytics'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_perm().
 */
function oga_downloads_perm() {
  return array(
    'view og downloads analytics',
  );
}



/**
 * Implements hook_og_analytics_navigation_links().
 * 
 * @param object $node
 */
function oga_downloads_og_analytics_navigation_links($node) {
  if (!$node || !isset($node->nid))
    return array();
  
  global $base_url;
  return array(
    $base_url . '/node/' . $node->nid . '/og/analytics/get_top_downloads_report' => 'Downloads'
  );
}


function oga_downloads_get_top_downloads_report($node) {

  drupal_set_title('Downloads Analytics');

  $output = '';

  drupal_add_js(drupal_get_path('module', 'oga_downloads') . '/js/oga_downloads.js');

  if (!isset($node->nid) && !og_is_group_type($node->type)) {
    if ($group_node = og_get_group_context()) {
      $node = $group_node;
    }
  }

  if (!$node)
    drupal_access_denied();

  $output .= drupal_get_form('oga_downloads_get_downloads_by_content_type_form', $node);
  $output .= drupal_get_form('oga_downloads_top_downloaded_content_form', $node);
  $output .= drupal_get_form('oga_downloads_top_downloading_users_form', $node);
  $output .= drupal_get_form('oga_downloads_file_download_users_form', $node);

  return theme('og_analytics', $output, 'Downloads', $node);

}





/**
 * Users list who downloaded a file.
 */
function oga_downloads_get_downloads_by_content_type_form(&$form_state, $node) {
  
  $form_id = 'downloads_by_content_type';

  // container div for the whole form
  $form['og_analytics'][$form_id]['filters'] = array(
    '#title' => t('Downloaders'),
    '#type' => 'fieldset',
    '#attributes' => array('class' => 'og_analytics'),
    '#collapsed' => FALSE,
    '#collapsible' => TRUE,
  );

  // form controls
  $form['og_analytics'][$form_id]['gid'] = array(
    '#type' => 'hidden',
    '#default_value' => $node->nid,
  );

  $node_types = node_get_types('names');
  $form['og_analytics'][$form_id]['filters']['content_type'] = array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#size' => 3,
    '#title' => t('Filter Content Type'),
    '#options' => $node_types,
    '#default_value' => $content_type_default,
  );

  $form['og_analytics'][$form_id]['filters']['date_start'] = array(
    '#type' => 'date_popup',
    '#title' => t('From'),
    '#date_format' => 'Y-m-d',
    '#date_year_range' => '-10:+0',
    '#default_value' => date('Y-m-d', strtotime('-30 days')),
  );

  $form['og_analytics'][$form_id]['filters']['date_end'] = array(
    '#type' => 'date_popup',
    '#title' => t('To'),
    '#date_format' => 'Y-m-d',
    '#date_year_range' => '-10:+0',
    '#default_value' => date('Y-m-d'),
  );

  $form['og_analytics'][$form_id]['filters'][$form_id . '_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Generate Report'),
    '#name' => $form_id . '_submit',
   );

  // Div markup.
  $form['og_analytics'][$form_id]['chart'] = array(
    '#type' => 'markup',
    '#value' => '<div class="og_analytics_chart" id="' . $form_id . '_chart"></div>',
  );

  $form['#attributes'] = array('class' => 'og_analytics_form');

  return $form;

}




/**
 * Callback.
 */
function oga_downloads_get_downloads_by_content_type() {
  
  $gid = $_POST['gid'];
  $date_start = $_POST['date_start'];
  $date_end = $_POST['date_end'];
  $content_types = $_POST['content_type'];

  // Setup the columns.
  $data = array(
    "cols" => array(
      array("id" => "", "label" => "Content Type", "pattern" => "", "type" => "string"),
      array("id" => "", "label" => "Download Count", "pattern" => "", "type" => "number"),
    ),
  );

  $date_start = strtotime($date_start);
  if (!$date_start)
    $date_start = strtotime('-30 days');

  if ($date_end)
    $date_end .= ' 23:59:59';
  $date_end = strtotime($date_end);
  if (!$date_end)
    $date_end = strtotime(date('Y-m-d 23:59:59'));

  if ((int) $limit > 100)
    $limit = 100;

  if (!$limit)
    $limit = 10;

  $node_types = json_decode($content_types);

  if (!$node_types || !is_array($node_types))
    $node_types = array_keys(node_get_types('names'));

  $query = '
    SELECT COUNT(dc.dcid) AS count, n.type
    FROM {node} n
    JOIN {download_count} dc ON dc.nid = n.nid
    JOIN {og_ancestry} oga ON oga.nid = n.nid
    WHERE
      n.type IN (' . db_placeholders($node_types, 'varchar') . ')
    AND
      oga.group_nid = %d
    AND
      ((dc.timestamp <= %d) AND (dc.timestamp >= %d))
    GROUP BY n.type
    ORDER BY count DESC
  ';

  $result = db_query($query, array_merge($node_types, array($gid), array($date_end), array($date_start)));

  $i = 0;
  while ($row = db_fetch_array($result)) {
    $data['rows'][$i]['c'][] = array('v' => $row['type'], 'f' => NULL);
    $data['rows'][$i]['c'][] = array('v' => (int) $row['count'], 'f' => NULL);
    $i++;
  }

  og_analytics_return_json($data);

}





/**
 * Users list who downloaded a file.
 */
function oga_downloads_file_download_users_form(&$form_state, $node) {
  
  $form_id = 'file_download_users';

  // container div for the whole form
  $form['og_analytics'][$form_id]['filters'] = array(
    '#title' => t('Downloaders'),
    '#type' => 'fieldset',
    '#attributes' => array('class' => 'og_analytics'),
    '#collapsed' => FALSE,
    '#collapsible' => TRUE,
  );

  // form controls
  $form['og_analytics'][$form_id]['gid'] = array(
    '#type' => 'hidden',
    '#default_value' => $node->nid,
  );

  $form['og_analytics'][$form_id]['filters']['file_name'] = array(
    '#type' => 'textfield',
    '#title' => t('File name'),
    '#size' => 15,
  );

  $form['og_analytics'][$form_id]['filters']['limit'] = array(
    '#type' => 'select',
    '#title' => t('Limit results'),
    '#options' => array('10' => '10', '50' => '50', '100' => '100'),
  );

  $form['og_analytics'][$form_id]['filters']['date_start'] = array(
    '#type' => 'date_popup',
    '#title' => t('From'),
    '#date_format' => 'Y-m-d',
    '#date_year_range' => '-10:+0',
    '#default_value' => date('Y-m-d', strtotime('-30 days')),
  );

  $form['og_analytics'][$form_id]['filters']['date_end'] = array(
    '#type' => 'date_popup',
    '#title' => t('To'),
    '#date_format' => 'Y-m-d',
    '#date_year_range' => '-10:+0',
    '#default_value' => date('Y-m-d'),
  );

  $form['og_analytics'][$form_id]['filters'][$form_id . '_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Generate Report'),
    '#name' => $form_id . '_submit',
   );

  // Div markup.
  $form['og_analytics'][$form_id]['chart'] = array(
    '#type' => 'markup',
    '#value' => '<div class="og_analytics_chart" id="' . $form_id . '_chart"></div>',
  );

  $form['#attributes'] = array('class' => 'og_analytics_form');

  return $form;

}




/**
 * Callback.
 */
function oga_downloads_get_file_download_users() {
  
  $gid = $_POST['gid'];
  $date_start = $_POST['date_start'];
  $date_end = $_POST['date_end'];
  $limit = $_POST['limit'];
  $file_name = $_POST['file_name'];

  // Setup the columns.
  $data = array(
    "cols" => array(
      array("id" => "", "label" => "Username", "pattern" => "", "type" => "string"),
      array("id" => "", "label" => "User ID", "pattern" => "", "type" => "string"),
      array("id" => "", "label" => "Email", "pattern" => "", "type" => "string"),
      array("id" => "", "label" => "Downloads", "pattern" => "", "type" => "number"),
    ),
  );

  $date_start = strtotime($date_start);
  if (!$date_start)
    $date_start = strtotime('-30 days');

  if ($date_end)
    $date_end .= ' 23:59:59';
  $date_end = strtotime($date_end);
  if (!$date_end)
    $date_end = strtotime(date('Y-m-d 23:59:59'));

  if ((int) $limit > 100)
    $limit = 100;

  if (!$limit)
    $limit = 10;

  $query = '
    SELECT
     DISTINCT(dc.uid) as uid, COUNT(dc.dcid) as count, u.name, u.mail
    FROM
      node n
    JOIN download_count dc ON dc.nid = n.nid
    JOIN users u ON u.uid = dc.uid
    WHERE
      n.title = "%s"
    AND
      ((dc.timestamp <= %d) AND (dc.timestamp >= %d))
    GROUP BY uid
    ORDER BY dc.timestamp DESC
    LIMIT %d
  ';

  $result = db_query($query, $file_name, $date_end, $date_start, $limit);

  $i = 0;
  while ($row = db_fetch_array($result)) {
    $data['rows'][$i]['c'][] = array('v' => $row['name'], 'f' => NULL);
    $data['rows'][$i]['c'][] = array('v' => $row['uid'], 'f' => NULL);
    $data['rows'][$i]['c'][] = array('v' => $row['mail'], 'f' => NULL);
    $data['rows'][$i]['c'][] = array('v' => (int) $row['count'], 'f' => NULL);
    $i++;
  }

  og_analytics_return_json($data);

}


/**
 * Users who downloaded the most files.
 * 
 * 2 options for results here:
 *   1. users who download the same file > 1 will be counted as 1 download.
 *   2. any download of users will count.
 */
function oga_downloads_top_downloading_users_form(&$form_state, $node) {
  
  $form_id = 'top_downloading_users';

  // Container div for the whole form.
  $form['og_analytics'][$form_id]['filters'] = array(
    '#title' => t('Top Downloaders'),
    '#type' => 'fieldset',
    '#attributes' => array('class' => 'og_analytics'),
    '#collapsed' => FALSE,
    '#collapsible' => TRUE,
  );

  // Form controls.
  $form['og_analytics'][$form_id]['gid'] = array(
    '#type' => 'hidden',
    '#default_value' => $node->nid,
  );

  $form['og_analytics'][$form_id]['filters']['download_type'] = array(
    '#type' => 'checkbox',
    '#title' => t('Unique Downloads'),
  );

  $form['og_analytics'][$form_id]['filters']['limit'] = array(
    '#type' => 'select',
    '#title' => t('Limit results'),
    '#options' => array('10' => '10', '50' => '50', '100' => '100'),
  );

  $form['og_analytics'][$form_id]['filters']['date_start'] = array(
    '#type' => 'date_popup',
    '#title' => t('From'),
    '#date_format' => 'Y-m-d',
    '#date_year_range' => '-10:+0',
    '#default_value' => date('Y-m-d', strtotime('-30 days')),
  );

  $form['og_analytics'][$form_id]['filters']['date_end'] = array(
    '#type' => 'date_popup',
    '#title' => t('To'),
    '#date_format' => 'Y-m-d',
    '#date_year_range' => '-10:+0',
    '#default_value' => date('Y-m-d'),
  );

  $form['og_analytics'][$form_id]['filters'][$form_id . '_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Generate Report'),
    '#name' => $form_id . '_submit',
   );

  // Div markup.
  $form['og_analytics'][$form_id]['chart'] = array(
    '#type' => 'markup',
    '#value' => '<div class="og_analytics_chart" id="' . $form_id . '_chart"></div>',
  );

  $form['#attributes'] = array('class' => 'og_analytics_form');

  return $form;

}


/**
 * Callback.
 * 
 */
function oga_downloads_get_top_downloading_users() {
  
  $gid = $_POST['gid'];
  $date_start = $_POST['date_start'];
  $date_end = $_POST['date_end'];
  $limit = $_POST['limit'];
  $download_type = $_POST['download_type'];

  /**
   * $download_type is one of:
   * '0' - All Downloads
   * '1' - Unique
   */

  // Setup the columns.
  $data = array(
    "cols" => array(
      array("id" => "", "label" => "Username ", "pattern" => "", "type" => "string"),
      array("id" => "", "label" => "User ID", "pattern" => "", "type" => "string"),
      array("id" => "", "label" => "Email ", "pattern" => "", "type" => "string"),
      array("id" => "", "label" => "Download Count ", "pattern" => "", "type" => "number"),
    ),
  );

  $date_start = strtotime($date_start);
  if (!$date_start)
    $date_start = strtotime('-30 days');

  if ($date_end)
    $date_end .= ' 23:59:59';
  $date_end = strtotime($date_end);
  if (!$date_end)
    $date_end = strtotime(date('Y-m-d 23:59:59'));

  if ((int) $limit > 100)
    $limit = 100;

  if (!$limit)
    $limit = 10;

  $download_type_sql = 'DISTINCT(dc.nid)';
  if ($download_type && $download_type == 1) {
    $download_type_sql = 'DISTINCT(dc.nid)';
  }
  else {
    $download_type_sql = 'dc.nid';
  }


  $query = '
    SELECT
     COUNT(%s) as count, dc.uid, u.name, u.mail
    FROM download_count dc
    JOIN node n ON n.nid = dc.nid
    JOIN og_ancestry oga ON dc.nid = oga.nid
    JOIN users u ON u.uid = dc.uid
    WHERE
      oga.group_nid = %d
    AND
      ((dc.timestamp <= %d) AND (dc.timestamp >= %d))
    GROUP BY dc.uid
    ORDER BY count DESC
    LIMIT %d
  ';

  $result = db_query($query, array_merge(array($download_type_sql), array($gid), array($date_end), array($date_start), array($limit)));

  $i = 0;
  while ($row = db_fetch_array($result)) {
    $data['rows'][$i]['c'][] = array('v' => $row['name'], 'f' => NULL);
    $data['rows'][$i]['c'][] = array('v' => $row['uid'], 'f' => NULL);
    $data['rows'][$i]['c'][] = array('v' => $row['mail'], 'f' => NULL);
    $data['rows'][$i]['c'][] = array('v' => (int) $row['count'], 'f' => NULL);
    $i++;
  }

  og_analytics_return_json($data);

}





function oga_downloads_get_totals_downloads_content_type_form(&$form_state, $node) {
 
  $form_id = 'top_downloads_employee_status';

  // Container div for the whole form.
  $form['og_analytics'][$form_id]['filters'] = array(
    '#title' => t('Total Downloads'),
    '#type' => 'fieldset',
    //'#attributes' => array('class' => $form_id),
    '#attributes' => array('class' => 'og_analytics'),
    '#collapsed' => FALSE,
    '#collapsible' => TRUE,
  );

  // Form controls.
  $form['og_analytics'][$form_id]['gid'] = array(
    '#type' => 'hidden',
    '#default_value' => $node->nid,
  );

  $form['og_analytics'][$form_id]['filters']['date_start'] = array(
    '#type' => 'date_popup',
    '#title' => t('From'),
    '#date_format' => 'Y-m-d',
    '#date_year_range' => '-10:+0',
    '#default_value' => date('Y-m-d', strtotime('-30 days')),
  );

  $form['og_analytics'][$form_id]['filters']['date_end'] = array(
    '#type' => 'date_popup',
    '#title' => t('To'),
    '#date_format' => 'Y-m-d',
    '#date_year_range' => '-10:+0',
    '#default_value' => date('Y-m-d'),
  );

  $form['og_analytics'][$form_id]['filters'][$form_id . '_submit'] = array(
    '#type' => '>',
    '#value' => t('Generate Report'),
    '#name' => $form_id . '_submit',
   );

  // Div markup.
  $form['og_analytics'][$form_id]['chart1'] = array(
    '#type' => 'markup',
    '#value' => '<div class="og_analytics_chart_container"> <div class="og_analytics_chart_float" id="' . $form_id . '_chart2"></div> <div class="og_analytics_chart_float" id="' . $form_id . '_chart1"></div> </div>',
  );

  $form['#attributes'] = array('class' => 'og_analytics_form');

  return $form;
  
}



function oga_downloads_get_totals_downloads_content_type() {
  
  $gid = $_POST['gid'];
  $node_types = $_POST['content_type'];
  $date_start = $_POST['date_start'];
  $date_end = $_POST['date_end'];

  // Setup the columns.
  $data = array(
    "cols" => array(
      array("id" => "", "label" => "Content Type", "pattern" => "", "type" => "string"),
      array("id" => "", "label" => "Total Downloads", "pattern" => "", "type" => "number"),
    ),
  );

  $date_start = strtotime($date_start);
  if (!$date_start)
    $date_start = strtotime('-30 days');

  if ($date_end)
    $date_end .= ' 23:59:59';
  $date_end = strtotime($date_end);
  if (!$date_end)
    $date_end = strtotime(date('Y-m-d 23:59:59'));

  if (!$node_types || !is_array($node_types))
    $node_types[] = 'other_content_file';

  $query = '
    SELECT
      COUNT(DISTINCT(dc.dcid)) as count, n.type as content_type
    FROM download_count dc
    JOIN node n ON n.nid = dc.nid
    JOIN og_ancestry oga ON dc.nid = oga.nid
    JOIN sec_grps_user sgu ON sgu.uid = dc.uid
    WHERE
      n.type IN (' . db_placeholders($node_types, 'varchar') . ')
    AND
     oga.nid = %d
    AND
      ((dc.timestamp <= %d) AND (dc.timestamp >= %d))
  ';

  $result = db_query($query, array_merge($node_types, array($gid), array($date_end), array($date_start)));

  $i = 0;
  while ($row = db_fetch_array($result)) {
    $data['rows'][$i]['c'][] = array('v' => $row['content_type'], 'f' => NULL);
    $data['rows'][$i]['c'][] = array('v' => (int) $row['count'], 'f' => NULL);
    $i++;
  }

  og_analytics_return_json($data);

}




function oga_downloads_top_downloaded_content_form(&$form_state, $node) {

  $form_id = 'top_downloaded_content';

  // Container div for the whole form.
  $form['og_analytics'][$form_id]['filters'] = array(
    '#title' => t('Top Downloads'),
    '#type' => 'fieldset',
    '#attributes' => array('class' => 'og_analytics'),
    '#collapsed' => FALSE,
    '#collapsible' => TRUE,
  );

  // Form controls.
  $form['og_analytics'][$form_id]['gid'] = array(
    '#type' => 'hidden',
    '#default_value' => $node->nid,
  );

  $node_types = node_get_types('names');
  $form['og_analytics'][$form_id]['filters']['content_type'] = array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#size' => 3,
    '#title' => t('Filter Content Type'),
    '#options' => $node_types,
    '#default_value' => $content_type_default,
  );

  $form['og_analytics'][$form_id]['filters']['limit'] = array(
    '#type' => 'select',
    '#title' => t('Limit results'),
    '#options' => array('10' => '10', '50' => '50', '100' => '100'),
  );

  $form['og_analytics'][$form_id]['filters']['date_start'] = array(
    '#type' => 'date_popup',
    '#title' => t('From'),
    '#date_format' => 'Y-m-d',
    '#date_year_range' => '-10:+0',
    '#default_value' => date('Y-m-d', strtotime('-30 days')),
  );

  $form['og_analytics'][$form_id]['filters']['date_end'] = array(
    '#type' => 'date_popup',
    '#title' => t('To'),
    '#date_format' => 'Y-m-d',
    '#date_year_range' => '-10:+0',
    '#default_value' => date('Y-m-d'),
  );

  $form['og_analytics'][$form_id]['filters'][$form_id . '_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Generate Report'),
    '#name' => $form_id . '_submit',
   );

  // Div markup.
  $form['og_analytics'][$form_id]['chart'] = array(
    '#type' => 'markup',
    '#value' => '<div class="og_analytics_chart" id="' . $form_id . '_chart" ></div>',
  );

  $form['#attributes'] = array('class' => 'og_analytics_form');

  return $form;
}


function oga_downloads_get_top_downloaded_content() {

  $gid = $_POST['gid'];
  $content_types = $_POST['content_type'];
  $date_start = $_POST['date_start']['date'];
  $date_end = $_POST['date_end']['date'];
  $limit = $_POST['limit'];

  $node_types = json_decode($content_types);

  $date_start = strtotime($date_start);
  if (!$date_start)
    $date_start = strtotime('-30 days');

  if ($date_end)
    $date_end .= ' 23:59:59';
  $date_end = strtotime($date_end);
  if (!$date_end)
    $date_end = strtotime(date('Y-m-d 23:59:59'));

  if (!$node_types || !is_array($node_types))
    $node_types = array_keys(node_get_types('names'));

  if ((int) $limit > 100)
    $limit = 100;

  if (!$limit)
    $limit = 10;

  $query = '
  SELECT
   COUNT(dc.dcid) as count, dc.nid as nid, dc.fid as fid, n.title as title, group_info.title as group_title
  FROM download_count dc
  JOIN node n ON n.nid = dc.nid
  JOIN og_ancestry oga ON dc.nid = oga.nid
  JOIN node group_info ON group_info.nid = oga.group_nid
  WHERE
    oga.group_nid = %d
  AND
    n.type IN (' . db_placeholders($node_types, 'varchar') . ')
  AND
    ((dc.timestamp <= %d) AND (dc.timestamp >= %d))
  GROUP BY dc.nid
  ORDER BY count DESC
  LIMIT %d
  ';

  $result = db_query($query, array_merge(array($gid), $node_types, array($date_end), array($date_start), array($limit)));

  // Setup the columns.
  $data = array(
    "cols" => array(
      array("id" => "", "label" => "Content Name", "pattern" => "", "type" => "string"),
      array("id" => "", "label" => "Node ID", "pattern" => "", "type" => "string"),
      array("id" => "", "label" => "File ID", "pattern" => "", "type" => "number"),
      array("id" => "", "label" => "Downloads", "pattern" => "", "type" => "number"),
    )  ,
  );


  // Setup the rows.
  $i = 0;
  while ($row = db_fetch_array($result)) {
    $data['rows'][$i]['c'][] = array('v' => $row['title'], 'f' => NULL);
    $data['rows'][$i]['c'][] = array('v' => (int) $row['nid'], 'f' => NULL);
    $data['rows'][$i]['c'][] = array('v' => (int) $row['fid'], 'f' => NULL);
    $data['rows'][$i]['c'][] = array('v' => (int) $row['count'], 'f' => NULL);
    $i++;
  }

  og_analytics_return_json($data);

}