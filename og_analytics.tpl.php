<?php
/**
 * @file og_analytics.tpl.php
 * Default theme implementation to display an og analytics report
 *
 * Variables available:
 * - $navigation: Navigation HTML
 * - $content: The actual report HTML content - forms and their charts
 * 
 * @see template_preprocess_og_analytics()
 * @see theme_og_analytics()
 */
?>

	<?php
		$form = drupal_get_form('og_analytics_navigation_form', $node, $navigation_current);
		print($form);
	?>

<?php if ($content): ?>
	<?php print $content ?>
<?php endif; ?>
