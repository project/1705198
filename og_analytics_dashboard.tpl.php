<?php

/**
 * @file og_analytics_dashboard.tpl.php
 * Default theme implementation to display an og analytics dashboard
 *
 * Variables available:
 * - $content: The actual report HTML content - forms and their charts
 * 
 * @see template_preprocess_og_analytics()
 * @see theme_og_analytics()
 */
?>

<hr/>

	<fieldset class='og_analytics'>
		<div class='fieldset-wrapper'>

<?php
	foreach($content as $widgets):
		foreach($widgets as $widget):
	
			if (isset($widget['html'])):
				print $widget['html'];
			else:
?>

			<div class='widget'>

				<label for=''>
					<?php print $widget['data'] ?>
				</label>

				<p>
					<?php print $widget['description'] ?>
				</p>

			</div>

			<?php endif;?>
		
	<?php
		endforeach;
	endforeach;
?>


		</div>
	</fieldset>