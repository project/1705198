<?php

/**
 * @file og_analytics_widget.tpl.php
 * Default theme implementation to display an og analytics widget
 *
 * Variables available:
 * - $content: The widget content
 * 
 * @see template_preprocess_og_analytics_widget()
 * @see theme_og_analytics_widget()
 */
?>

<div class='widget'>

	<label for=''>
		<?php print $content['data'] ?>
	</label>

	<p>
		<?php print $content['description'] ?>
	</p>

</div>
