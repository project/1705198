<?php

/**
 * @file og_analytics_widget_members_count.tpl.php
 * Default theme implementation to display an og analytics widget
 *
 * Variables available:
 * - $content: The widget content
 * 
 * @see template_preprocess_og_analytics_widget_members_count()
 * @see theme_og_analytics_widget_members_count()
 */
?>

<div class='widget'>

	<?php
		global $base_url; 
		$img_url = $base_url . '/sites/all/modules/og_analytics/images/members_count.png';
	?>

	<img src="<?php echo $img_url; ?>" />

	<label for=''>
		<?php print $content ?>
	</label>

	<p>
		Members
	</p>

</div>
